class Bank {
  constructor() {
    this.accounts = []
  }

  getAccount(name) {
    let index = this.accounts.findIndex(acc => acc.name == name)
    if(index != -1) {
      return index
    } else {
      throw 'Account doesn\'t exist!'
    }
  }

  createAccount(name) {
    if(!name) {
      throw 'Account must have a name!'
    } else if(this.accounts.findIndex(acc => acc.name == name) != -1) {
      throw 'Account already exists!'
    } else {
      this.accounts.push(new Account(name))
    }
  }

  loginAccount(name) {
    return this.getAccount(name) >= 0
  }

  balance(name) {
    let index = this.getAccount(name)
    return this.accounts[index].getBalance()
  }

  deposit(name, amount) {
    let index = this.getAccount(name)
    this.accounts[index].deposit(amount)
  }

  withdraw(name, amount) {
    let index = this.getAccount(name)
    this.accounts[index].withdraw(amount)
  }

  history(name) {
    let index = this.getAccount(name)
    return this.accounts[index].getHistory()
  }
}

class Account {
  constructor(name) {
    this.name = name
    this.history = []
    this.balance = 0.0
  }

  logHistory(message) {
    this.history.push(`[${new Date().toString()}] ${message}`)
  }

  getHistory() {
    return this.history
  }

  getBalance() {
    return Number(this.balance)
  }

  deposit(amount) {
    if(amount <= 0) {
      throw 'Amount must be a non-zero positive value!'
    } else {
      this.balance += amount
      this.logHistory(`Deposit of $${amount}`)
    }
  }

  withdraw(amount) {
    if(amount <= 0) {
      throw 'Amount must be a non-zero positive value!'
    } else if(amount > this.balance) {
      throw 'Amount must not exceed account balance!'
    } else {
      this.balance -= amount
      this.logHistory(`Withdrawal of $${amount}`)
    }
  }
}

module.exports = Bank