const readline = require('readline')

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  prompt: '> '
})

rl.on('close', () => process.exit())

let currentUser = null

init = () => {
  rl.question(`
    Welcome to the world's greatest bank ledger! How would you like to begin?
    1) Login
    2) Create an account
    3) Quit
    `, answer => {
      switch(answer) {
        case '1':
          login()
          break
        case '2':
          create()
          break
        case '3':
          console.log('*** Goodbye!')
          rl.close()
          break
        default:
          init()
      }
    })
}

login = () => {
  rl.question(`
    Name of account? (leave empty to go back)
    `, name => {
      try {
        if(name == '') {
          init()
        } else {
          global.bank.loginAccount(name)
          currentUser = name
          console.log('*** Successfully logged in!')
          main()
        }
      } catch(err) {
        console.log('!!! ' + err)
        login()
      }
    })
}

create = () => {
  rl.question(`
    Enter a name for your new account (leave empty to cancel)
    `, name => {
      try {
        if(name != '') {
          global.bank.createAccount(name)
          console.log('*** Account created! Please log in now.')
        }
        init()
      } catch(err) {
        console.log('!!! ' + err)
        create()
      }
    })
}

deposit = () => {
  rl.question(`
    Enter amount (-1 to cancel)
    `, amount => {
      try {
        if(amount != '-1') {
          global.bank.deposit(currentUser, Number(amount))
          console.log(`*** Successfully deposited $${amount} into your account`)
        }
        main()
      } catch(err) {
        console.log('!!! ' + err)
        deposit()
      }
    })
}

withdraw = () => {
  rl.question(`
    Enter amount (-1 to cancel)
    `, amount => {
      try {
        if(amount != '-1') {
          global.bank.withdraw(currentUser, Number(amount))
          console.log(`*** Successfully withdrew $${amount} from your account`)
        }
        main()
      } catch(err) {
        console.log('!!! ' + err)
        withdraw()
      }
    })
}

main = () => {
  rl.question(`
    What would you like to do?
    1) Check balance
    2) Deposit
    3) Withdraw
    4) View transaction history
    5) Log out
    `, answer => {
      try {
        switch(answer) {
          case '1':
            console.log('*** Your current balance is $' + global.bank.balance(currentUser).toFixed(2))
            break
          case '2':
            deposit()
            break
          case '3':
            withdraw()
            break
          case '4':
            console.log('***\n' + global.bank.history(currentUser).join('\n') + '\n***')
            break
          case '5':
            currentUser = null
            console.log('*** You are now logged out!')
            init()
            break
          default:
            main()
        }
      } catch(err) {
        console.log('!!! ' + err)
      }
      main()
    })
}

init()