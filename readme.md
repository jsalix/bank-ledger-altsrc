## Code sample for AltSource

Node.js is required to run: https://nodejs.org/

#### First time setup:
```
npm install
```

#### Start the application with:
```
npm start
```