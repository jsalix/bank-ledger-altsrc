const Express = require('express')

const Templates = require('./templates')
const Bank = require('./bank')
global.bank = new Bank()

let app = Express()

app
  .get('/', (req, res) => {
    if(req.query.created) {
      res.send(Templates.init('Successfully created account! Please log in now.'))
    } else if(req.query.logout) {
      res.send(Templates.init('Successfully logged out!'))
    } else {
      res.send(Templates.init())
    }
  })
  .get('/login', (req, res) => {
    if(req.query.name) {
      try {
        global.bank.loginAccount(req.query.name)
        res.redirect(`/account/${req.query.name}`)
      } catch(err) {
        res.send(Templates.login(err))
      }
    } else {
      res.send(Templates.login())
    }
  })
  .get('/create', (req, res) => {
    if(req.query.name) {
      try {
        global.bank.createAccount(req.query.name)
        res.redirect('/?created=success')
      } catch(err) {
        res.send(Templates.create(err))
      }
    } else {
      res.send(Templates.create())
    }
  })
  .get('/account/:name', (req, res) => {
    if(req.params.name) {
      try {
        let balance = global.bank.balance(req.params.name).toFixed(2)
        res.send(Templates.main(req.params.name, balance))
      } catch(err) {
        res.redirect('/')
      }
    } else {
      res.redirect('/')
    }
  })
  .get('/deposit/:name', (req, res) => {
    if(req.params.name) {
      if(req.query.amount) {
        try {
          global.bank.deposit(req.params.name, Number(req.query.amount))
          res.redirect(`/account/${req.params.name}`)
        } catch(err) {
          res.send(Templates.deposit(req.params.name, err))
        }
      } else {
        res.send(Templates.deposit(req.params.name))
      }
    } else {
      res.redirect('/')
    }
  })
  .get('/withdraw/:name', (req, res) => {
    if(req.params.name) {
      if(req.query.amount) {
        try {
          global.bank.withdraw(req.params.name, Number(req.query.amount))
          res.redirect(`/account/${req.params.name}`)
        } catch(err) {
          res.send(Templates.withdraw(req.params.name, err))
        }
      } else {
        res.send(Templates.withdraw(req.params.name))
      }
    } else {
      res.redirect('/')
    }
  })
  .get('/history/:name', (req, res) => {
    if(req.params.name) {
      let history = global.bank.history(req.params.name).join('<br>')
      res.send(Templates.history(req.params.name, history))
    } else {
      res.redirect('/')
    }
  })

app.listen(8080)
console.log('(web interface available at localhost:8080)')

require('./terminal')