const init = (info = '') => `
<p style="color: green;">${info}</p>
Welcome to the world's greatest bank ledger! How would you like to begin?<br>
<a href="/login">Login</a><br>
<a href="/create">Create an account</a><br>
`

const login = (err = '') => `
Name of account?
<form>
<input type="text" name="name"><br>
<input type="submit" value="Login">
</form>
<p style="color: red;">${err}</p>
<br><br><a href="/">Go back</a>
`

const create = (err = '') => `
Enter a name for your new account
<form>
<input type="text" name="name"><br>
<input type="submit" value="Create">
</form>
<p style="color: red;">${err}</p>
<br><br><a href="/">Go back</a>
`

const main = (name, balance) => `
Your current balance is $${balance}<br><br>
What would you like to do?<br>
<a href="/deposit/${name}">Deposit</a><br>
<a href="/withdraw/${name}">Withdraw</a><br>
<a href="/history/${name}">View transaction history</a><br><br>
<a href="/?logout=success">Log out</a>
`

const deposit = (name, err = '') => `
Enter amount to deposit
<form>
<input type="text" name="amount"><br>
<input type="submit" value="Deposit">
</form>
<p style="color: red;">${err}</p>
<br><br><a href="/account/${name}">Go back</a>
`

const withdraw = (name, err = '') => `
Enter amount to withdraw
<form>
<input type="text" name="amount"><br>
<input type="submit" value="Withdraw">
</form>
<p style="color: red;">${err}</p>
<br><br><a href="/account/${name}">Go back</a>
`

const history = (name, history) => `
<p>${history}</p>
<br><a href="/account/${name}">Go back</a>
`

module.exports = {init, login, create, main, deposit, withdraw, history}